
# 1. Relack Clone

React-Native (RN) clone of slack of the mobile version for iOS and Android

## Setting your enviroment


You will need the following tools:

- NodeJS and npm: [NodeJS](https://nodejs.org/en/) and npm version 4.6.1 (react-native/expo is not compatible with npm 5) which means you have to run
`npm install npm@4.6.1`
after installing NodeJS

- VScode editor to edit your code


- Need the Expo Client/Xcode\*/Android Studio* to demo the app. Go to your Apple/Android store and type `expo` and download the expo client app. 


- After installing npm you need to run `npm install -g create-react-native-app` (you can also use yarn) to intall the node package which will set our React-Native/Expo project. 

- Once you install the **create-react-native-app** package you can run the following command to get started. 
`create-react-native-app <your project name>`

# 2. Files/Directories

### File Structure
You may set up your project in whatever fashion you'd like but the webpack file structure is very popular amongs many JavaScript frameworks

- node_modules: Where the application's dependencies are installed.
- src: Here the application lives. This folder will contain all of the application's components.  We will spend almost all of our time working within this folder.
- static: This folder is where any static assets can be stored (pictures, videos, etc). 
- package.json: This file is where all meta-data regarding our application is defined along with where our application's dependencies are declared.
- App.js: This is our application's entry point. The RN app we develop will be embedded within this document's body tags.

# 3. General React Native Settings 

## Available Scripts

### `npm start`

Runs your app in development mode.

Open it in the [Expo app](https://expo.io) on your phone to view it. It will reload if you save edits to your files, and you will see build errors and logs in the terminal.

Sometimes you may need to reset or clear the React Native packager's cache. To do so, you can pass the `--reset-cache` flag to the start script:

```
npm start -- --reset-cache
# or
yarn start -- --reset-cache
```
To stop the server simply use `ctrl-c`

#### `npm run ios`

Like `npm start`, but also attempts to open your app in the iOS Simulator if you're on a Mac and have it installed.

#### `npm run android`

Like `npm start`, but also attempts to open your app on a connected Android device or emulator. Requires an installation of Android build tools (see [React Native docs](https://facebook.github.io/react-native/docs/getting-started.html) for detailed setup). We also recommend installing Genymotion as your Android emulator. Once you've finished setting up the native build environment, there are two options for making the right copy of `adb` available to Create React Native App:

##### Using Android Studio's `adb`

1. Make sure that you can run adb from your terminal.
2. Open Genymotion and navigate to `Settings -> ADB`. Select “Use custom Android SDK tools” and update with your [Android SDK directory](https://stackoverflow.com/questions/25176594/android-sdk-location).

##### Using Genymotion's `adb`

1. Find Genymotion’s copy of adb. On macOS for example, this is normally `/Applications/Genymotion.app/Contents/MacOS/tools/`.
2. Add the Genymotion tools directory to your path (instructions for [Mac](http://osxdaily.com/2014/08/14/add-new-path-to-path-command-line/), [Linux](http://www.computerhope.com/issues/ch001647.htm), and [Windows](https://www.howtogeek.com/118594/how-to-edit-your-system-path-for-easy-command-line-access/)).
3. Make sure that you can run adb from your terminal.

#### `npm run eject`

This will start the process of "ejecting" from Create React Native App's build scripts. You'll be asked a couple of questions about how you'd like to build your project.

**Warning:** Running eject is a permanent action (aside from whatever version control system you use). An ejected app will require you to have an [Xcode and/or Android Studio environment](https://facebook.github.io/react-native/docs/getting-started.html) set up.

## Customizing App Display Name and Icon

You can edit `app.json` to include [configuration keys](https://docs.expo.io/versions/latest/guides/configuration.html) under the `expo` key.

To change your app's display name, set the `expo.name` key in `app.json` to an appropriate string.

To set an app icon, set the `expo.icon` key in `app.json` to be either a local path or a URL. It's recommended that you use a 512x512 png file with transparency.


# 4. Building a channel

## Add a few useful packages to your package.json

```
    "react-native-device-detection": "^0.1.3",
    "react-native-elements": "^0.18.5",
    "react-native-responsive-dimensions": "^1.0.2",
    "react-native-vector-icons": "^4.5.0",
    "react-navigation": "^1.0.0-beta.22"

``` 
make sure to run `npm install` in order to add these packages to your node_modules. 

## Run `npm start` to start your local/lan server and use your phone to scan the QR code to see the app on your personal device. 



# Mobile Nagivation

Once we demo our app, we see that the text displaying is telling us to modify our App.js file which we will do. Before that lets review some of the default generated code. Review what the components in `App.js` are doing to the app. 

Open `App.js` in the root directory

add the following code. 

```
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import Device from 'react-native-device-detection'
import ChannelPage from './src/channel.js';
import ChannelPage2 from './src/channel2.js';
import SideMenu from './src/sidemenu.js';
import {DrawerNavigator} from 'react-navigation';




const App = DrawerNavigator({
  blackbriar: { 
    screen: ChannelPage },
  ecmc: { 
    screen: ChannelPage2 },
}, 
);


export default App;
```

Next, create the following files in your **src** folder: channel.js and channel2.js

### channel.js

```
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import Device from 'react-native-device-detection'

export default class ChannelPage extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <Text>Changes you make will automatically reload.</Text>
        <Text>Shake your phone to open the developer menu.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
``` 

### channel2.js
```
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import Device from 'react-native-device-detection'

export default class ChannelPage2 extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>This is ECMC Channel</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
```
###

Now you should have a general drawer navigation page that it's native to its operating system.  

## Let's digging on some more RN Components!

So far our application has a generic drawer navigation page so we need to make use of the other components that RN offers. Let's customize it to make it look like Slack. 

### Create a new component

- Let's create the file named `sidemenu.js`

- Let's invoke the libraries needed from RN

```
import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {ScrollView, Text, View, StyleSheet} from 'react-native';

```

Let's also create a class named SideMenu that extends React.Component

```
class SideMenu extends Component {
  navigateToScreen = (route) => () => {
  const navigateAction = NavigationActions.navigate({
    routeName: route
  });
  this.props.navigation.dispatch(navigateAction);

  render(){

  }
}

export default SideMenu;
}
```

###

Let's put something in our render function 

```
return (
      <View style={styles.container}>

        <ScrollView>
        <View>
            <View>
                <Text>Channels</Text>
            </View>
            <View>
                <Text>#blackbriar</Text>
            </View>
        </View>
        </ScrollView>

        <View>
          <Text>This is my fixed footer</Text>
        </View>

      </View>
    );
```

Simple HTML/VueJS like syntax, and CSS type styling. 

add
```
SideMenu.propTypes = {
  navigation: PropTypes.object
};



const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#49176d',
    },
    sectionHeadingStyle:{
      color: 'gray',
      marginTop: 50,
      fontSize: 20
    },
    navSectionStyle:{
        flex:1, 
    },
    navItemStyle:{
        color: 'white',
        
    }

  });
```