import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import Device from 'react-native-device-detection'
import ChannelPage from './src/channel.js';
import ChannelPage2 from './src/channel2.js';
import SideMenu from './src/sidemenu.js';
import {DrawerNavigator} from 'react-navigation';




const App = DrawerNavigator({
  blackbriar: { 
    screen: ChannelPage },
  ecmc: { 
    screen: ChannelPage2 },
}, 

);


export default App;