import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import Device from 'react-native-device-detection'

export default class ChannelPage2 extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>This is ECMC Channel</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });