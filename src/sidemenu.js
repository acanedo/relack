import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {ScrollView, Text, View, StyleSheet} from 'react-native';

class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render () {
    return (
      <View style={styles.container}>
        <ScrollView>
        <View style={{flex:1}}>
            <View style={{flex:1, flexDirection: 'row'}}>
                <View style={{flex:9}}>
                    <Text style={styles.sectionHeadingStyle}>
                        CHANNELS
                    </Text>
                </View>
                <View style={{flex:1}}>
                    <Text style={styles.sectionHeadingStyle}>
                        +
                    </Text>
                </View>
            </View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('blackbriar')}>
                #blackbriar
              </Text>
            </View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('ecmc')}>
                #ecmc
              </Text>
            </View>
          </View>
        </ScrollView>
        <View style={styles.footerContainer}>
          <Text>This is my fixed footer</Text>
        </View>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};



const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#49176d',
    },
    sectionHeadingStyle:{
      color: 'gray',
      marginTop: 50,
      fontSize: 20
    },
    navSectionStyle:{
        flex:1, 
    },
    navItemStyle:{
        color: 'white',
        
    }

  });
export default SideMenu;